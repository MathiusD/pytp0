chars = {
    'luke': {'job': 'jedi', 'name': 'Luke Skywalker', },
    'han': {'job': 'smuggler', 'name': 'Han Solo', },
    'ben': {'job': 'jedi', 'name': 'Ben Kenobi', },
}

print(chars['luke'])
print(chars['luke']['name'])
