chars = {
    'luke': {'job': 'jedi', 'name': 'Luke Skywalker', },
    'han': {'job': 'smuggler', 'name': 'Han Solo', },
    'ben': {'job': 'jedi', 'name': 'Ben Kenobi', },
}

print([value['name'] for key, value in chars.items() if value['job'] == 'jedi'])
