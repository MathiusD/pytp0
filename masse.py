object = 42
G = {
    'earth' : 1.00,
    'mercury' : 0.378,
    'the moon' : 0.166,
    'jupiter' : 2.364,
}
for key, value in G.items():
    sentence = "%s %s %s %f%s"%('The mass of the object on', key, 'is', object*G[key], '.')
    print(sentence)
