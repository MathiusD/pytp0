def say_hello():
    """
    This function only say hello
    Nothing less, nothing more
    But this is a cute doc string.
    """
    print("Hello") # What a complex line of code
